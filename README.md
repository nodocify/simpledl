# Simpledl

This repo tracks the progress of my 'simpledl' project.


# Project Goal:

I don't like using flash drives. They are sometimes never large enough, or I can't find them, or its the wrong type (USB A vs USB C), or they are full of old files from previous work. I also don't want to configure or install any extra utilities/libraries on any workstation just to support file sharing.

Therefore, I set out to create a single, cross platform, binary tool that serves a webpage. From that webpage you can upload or download any file from the directory that binary was ran. Enabling simple file sharing from my workstation directly across the LAN.


## Todo:

- Flesh out the web template so that you don't have to go to either the /upload or /download paths.
- Improve the web template to have a modern professional look.
- Improve the interface recommendation to use system routing metrics for priority.
- Add timeout, application should stop serving after XX minutes of idle.
- Add warning if application is ran with elevated privileges.
- Add CLI output detailing connected client and upload/download stats.
- Digitally sign the resulting binary.
- Create documentation.


## Additional Ideas

Note: These might never come to fruition. Was just excited about possibilities.

- Add integration for ngrok to allow internet access. (Restric to download only if this feature is used.)
- Have the application generate a self signed certificate on first run.
