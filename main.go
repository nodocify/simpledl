package main

import (
	"embed"
	"flag"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"log"
	"net"
	"net/http"
	"os"
)

const (
	version      = "1.0"
	templatesDir = "templates"
)

var (
	bind         = flag.String("b", "0.0.0.0", `Bind to (listen on) a specific interface. "0.0.0.0" is for ALL interfaces. "localhost" disables access from other devices.`)
	directory    = flag.String("d", ".", "The directory of static files to host")
	help         = flag.Bool("h", false, "Print the usage")
	port         = flag.String("p", "8080", "Port to serve on.")
	test         = flag.Bool("t", false, "Test / dry run (just prints the interface table)")
	printVersion = flag.Bool("v", false, "Print the version")

	//go:embed templates/*
	files     embed.FS
	templates map[string]*template.Template
)

func LoadTemplates() error {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	tmplFiles, err := fs.ReadDir(files, templatesDir)
	if err != nil {
		return err
	}

	for _, tmpl := range tmplFiles {
		if tmpl.IsDir() {
			continue
		}

		pt, err := template.ParseFS(files, templatesDir+"/"+tmpl.Name())
		if err != nil {
			return err
		}

		templates[tmpl.Name()] = pt
	}
	return nil
}

// Display the named template
func display(w http.ResponseWriter, page string, data interface{}) {
	t, err := templates[page+".html"]
	if !err {
		log.Fatal("Template not found.")
		return
	}
	t.Execute(w, data)
}

func uploadFile(w http.ResponseWriter, r *http.Request) {

	// 32 MB is the default used by FormFile()
	if err := r.ParseMultipartForm(32 << 20); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	files := r.MultipartForm.File["file"]

	for _, handler := range files {

		// open the file
		file, err := handler.Open()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		defer file.Close()
		fmt.Printf("Uploaded File: %+v\n", handler.Filename)
		fmt.Printf("File Size: %+v\n", handler.Size)
		fmt.Printf("MIME Header: %+v\n", handler.Header)

		// Create file
		dst, err := os.Create(handler.Filename)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer dst.Close()
		// Copy the uploaded file to the created file on the filesystem
		if _, err := io.Copy(dst, file); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Printf("\nFile upload complete.")
	}

	fmt.Fprintf(w, "Successfully Uploaded All Files\n")
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		display(w, "upload", nil)
	case "POST":
		uploadFile(w, r)
	}
}

func printAddrs(port string) {
	fmt.Println("\nLocal network interfaces and their IP addresses so you can pass one to your colleagues:")
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatal(err)
	}
	// We want the interface + IP address list to look like a table with a width of 80:
	//       Interface      |  IPv4 Address   |              IPv6 Address
	// ---------------------|-----------------|----------------------------------------
	// vEthernet (Standan.. | 192.168.178.123 | 1a23:850a:bf55:39a9:6dae:c378:9deb:5aff
	fmt.Println("      Interface      |  IPv4 Address   |              IPv6 Address              ")
	fmt.Println("---------------------|-----------------|----------------------------------------")
	fav := ""
	for _, iface := range ifaces {
		fmt.Printf("%-20v |", cutString(iface.Name, 20))

		// Select IPv4 and IPv6 address
		ipv4, ipv6 := getAddressesFromIface(iface)

		// If there's no favorite IPv4 address yet, check if we should pick the current one
		if fav == "" && isFav(iface) {
			fav = ipv4
		}

		fmt.Printf(" %-15v | %v\n", ipv4, ipv6)
	}

	// Show probable favorite
	if fav != "" {
		fmt.Printf("\nYou probably want to share:\nhttp://%v:%v\n", fav, port)
	}
}

func main() {

	flag.Parse()

	if *help {
		flag.CommandLine.SetOutput(os.Stdout)
		flag.Usage()
		os.Exit(0)
	}
	fmt.Printf("SimpleDL Version %v\n", version)
	if *printVersion {
		os.Exit(0)
	}
	if !isFlagPassed("d") && flag.Arg(0) != "" && isDirAccessible(flag.Arg(0), false) {
		fmt.Printf("\nWARNING: You didn't use the \"-d\" flag, but a positional argument instead. It seems to be a valid directory though.\n")
		*directory = flag.Arg(0)
	}
	isDirAccessible(*directory, true)
	if *test {
		if *bind != "0.0.0.0" {
			fmt.Printf("\nNo need to print the network interface table, because simpledl will bind to %v as requested, making it reachable via:\nhttp://%v:%v\n", *bind, *bind, *port)
		} else {
			printAddrs(*port)
		}
		os.Exit(0)
	}
	if *bind == "0.0.0.0" {
		fmt.Printf("\nServing \"%s\" on all network interfaces (0.0.0.0) on port: %s\n", *directory, *port)
		// Print local network interfaces and their IP addresses
		printAddrs(*port)
	} else {
		fmt.Printf("\nServing \"%s\" on:\nhttp://%v:%s\n", *directory, *bind, *port)
	}

	LoadTemplates()

	// Download route
	http.Handle("/download/", http.StripPrefix("/download/", http.FileServer(http.Dir(*directory))))

	// Upload route
	http.HandleFunc("/upload", uploadHandler)

	//Listen on port 8080
	log.Fatal(http.ListenAndServe(*bind+":"+*port, nil))
}
